#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
    char str;
    FILE *fptr;
    char fname[1000];

    printf("\nEnter the file name :");
    scanf ("%s",fname);
    fptr=fopen(fname,"w");
    if(fptr==NULL)
    {
        printf("\nError in opening file!");
        exit(1);
    }

    printf("\nInput a sentence for the file : ");
    fgets(str, sizeof str, stdin);
    fprintf(fptr,"%c",str);

    fptr = fopen(fname, "r");
    if (fptr == NULL)
    {
        perror("\nError while opening the file.\n");
        exit(EXIT_FAILURE);
    }

    printf("\n\nThe contents of %s file are:\n", fname);

    while((str = gets(fptr)) != '\0')
        printf("%s", str);

    fclose(fptr);
    printf("\n The file %s created successfully...!!\n\n",fname);
    return 0;
}
